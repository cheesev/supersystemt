package org.SuperSystemT;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import dto.stock.StockContractStrengthDto;
import service.stock.StockContractStrengthService;
import strategy.stock.StockContractStrengthStgyImpl;
import util.DateUtil;
import util.MathUtil;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.is;

/**
 * Unit test for simple App.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="/applicationContext.xml")
public class AppTest 
{
	@Autowired
	private ApplicationContext context;
	@Autowired
	private StockContractStrengthService stockContractStrengthService;

	@Before
	public void setUp() {
		
	}
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */

	@Test
	public void queryTest() throws Exception {
		StockContractStrengthDto dto = stockContractStrengthService
				.getLastTimeStockContractStrengthDto("A000020");
		assertThat(dto.getCurPrice(),  is(5740));
		
		List<String> getList = stockContractStrengthService.getAllStockCodeList();
		assertThat(getList.size(), is(1129));
	}
	
	@Test
	public void getNearTimeDtoTest() throws Exception {
		List<StockContractStrengthDto> stockContractStrengthDtoList =  
				stockContractStrengthService.getStockContractStrengthDtoListByCode("A000020"); 
		assertThat(stockContractStrengthDtoList.get(231).getContractTime() , is("132522"));	
		
		StockContractStrengthStgyImpl app = new StockContractStrengthStgyImpl();
		StockContractStrengthDto testDto = 
				app.getNearTimeConStrDto(stockContractStrengthDtoList, 231, 60 * 10);
		
		assertThat(testDto.getContractTime(), is("131439"));
	}
	
	@Test
	public void dateUtilTest() throws Exception {
		
		assertThat(DateUtil.diffSec("132514", "132509"), is((long)5));
		assertThat(DateUtil.diffSec("132514", "132413"), is((long)61));
		assertThat(DateUtil.diffSec("142514", "132413"), is((long)3661));
	}
	
	@Test
	public void mathUtilTest() throws Exception {
		assertThat(MathUtil.getNetProfit(new BigDecimal("100"), new BigDecimal("2"), 2).toString(), is("98.00"));
//		assertThat(MathUtil.isPositiveNetProfit(new BigDecimal("5730"), new BigDecimal("2"), 2), is(true));
		assertThat(MathUtil.isBetween(new BigDecimal("0.87"), 0, 1), is(true));
		assertThat(MathUtil.isBetween(new BigDecimal("100.87"), 100, 105), is(true));
		assertThat(MathUtil.isBetween(new BigDecimal("100.87"), 101, 105), is(false));

	}
	

}
