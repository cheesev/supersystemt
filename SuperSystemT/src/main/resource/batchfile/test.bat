@echo off
setlocal enabledelayedexpansion

set "file_name=C:\Users\Jung\Desktop\file.txt"
set cmd_win_num=6																
set fcnt=0
FOR /F "usebackq delims=" %%a IN (%file_name%) DO SET /A fcnt+=1

echo %fcnt%

set /a num_in_one_cmd=%fcnt% / %cmd_win_num%

echo %num_in_one_cmd%


set count=0

for /f "tokens=*" %%a in (%file_name%) do (
	set "concat_str=%%a,!concat_str!"	
	set /a count+=1

	if !count! equ %cmd_win_num% (	
		echo !concat_str!
		set "concat_str="
		
	) else (		
		if !count! equ %fcnt% ( 
			echo !concat_str!
		)
	)
)