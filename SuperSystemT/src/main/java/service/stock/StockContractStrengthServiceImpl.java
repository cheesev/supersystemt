package service.stock;

import java.util.List;
import java.util.Map;

import mapper.stock.StockContractStrengthMapper;
import dto.stock.StockContractStrengthDto;

public class StockContractStrengthServiceImpl implements StockContractStrengthService {
	private StockContractStrengthMapper stockContractStrengthMapper;
	
	public void setStockContractStrengthMapper(
			StockContractStrengthMapper stockContractStrengthMapper) {
		this.stockContractStrengthMapper = stockContractStrengthMapper;
	}

	public List<StockContractStrengthDto> getStockContractStrengthDtoListByCode(
			String stockCode) {
		return this.stockContractStrengthMapper.getStockContractStrengthDtoListByCode(stockCode);
	}

	public List<String> getAllStockCodeList() {
		return this.stockContractStrengthMapper.getAllStockCodeList();
	}

	public StockContractStrengthDto getLastTimeStockContractStrengthDto(
			String stockCode) {
		return this.stockContractStrengthMapper.getLastTimeStockContractStrengthDto(stockCode);
	}

}
