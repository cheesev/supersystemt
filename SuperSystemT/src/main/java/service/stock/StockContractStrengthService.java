package service.stock;

import java.util.List;
import java.util.Map;

import dto.stock.StockContractStrengthDto;

public interface StockContractStrengthService {
	List<StockContractStrengthDto> getStockContractStrengthDtoListByCode(String stockCode);	
	List<String> getAllStockCodeList();	
	StockContractStrengthDto getLastTimeStockContractStrengthDto(String stockCode);
}
