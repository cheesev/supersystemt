package strategy.stock;

import java.util.List;
import java.util.Map;

public interface StockContractStrengthStgy {
	
	Map<Integer, int[]> analyze(List<String> stockCodeList, int deltaTime);

}
