package strategy.stock;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import service.stock.StockContractStrengthService;
import util.DateUtil;
import util.MathUtil;
import dto.stock.StockContractStrengthDto;

public class StockContractStrengthStgyImpl implements StockContractStrengthStgy {
	
	private StockContractStrengthService stockContractStrengthService;
	
	public void setStockContractStrengthService(
			StockContractStrengthService stockContractStrengthService) {
		this.stockContractStrengthService = stockContractStrengthService;
	}

	/*
	 * 
	 * deltaTime : 시간 변화량  
	 */
	public Map<Integer, int[]> analyze(List<String> stockCodeList, int deltaTime) {
		System.out.println( "Start Analyzing... ");
		int successCnt = 0;
		int failCnt = 0;
		Map<Integer, int[]> deltaConStrMap = new HashMap<Integer, int[]>();
	
		
		for (int deltaConStr = 50; deltaConStr <= 200; deltaConStr += 5) {
			for(String stockCode : stockCodeList) {
				List<StockContractStrengthDto> stockContractStrengthDtoList = stockContractStrengthService
						.getStockContractStrengthDtoListByCode(stockCode);
				StockContractStrengthDto lastStkConStrDto = stockContractStrengthService
						.getLastTimeStockContractStrengthDto(stockCode);

				for (int idx = 0; idx < stockContractStrengthDtoList.size(); idx++) {
					StockContractStrengthDto nearTimeStkConStrDto = getNearTimeConStrDto(
							stockContractStrengthDtoList, idx, deltaTime);
					if (nearTimeStkConStrDto != null
							&& MathUtil.isBetween(
									stockContractStrengthDtoList.get(idx).getContractStrength()
									.subtract(nearTimeStkConStrDto.getContractStrength()),
									deltaConStr - 5, deltaConStr)) { // calculate delta contract_strength
						if ( MathUtil.isPositiveNetProfit(stockContractStrengthDtoList.get(idx).getCurPrice()
																	, lastStkConStrDto.getCurPrice()
								                                    , MathUtil.defaultCommission
								                                    , 2 ))  {
							System.out.println("성공한 것 Dto : " + stockContractStrengthDtoList.get(idx));
							System.out.println("마지막 Dto : " + lastStkConStrDto);
							System.out.println("near time Dto  : " + nearTimeStkConStrDto);
							successCnt++;
						} else {
							failCnt++;
							System.out.println("실패 Dto  : " + stockContractStrengthDtoList.get(idx));
						}
					}
				}
				interResult(stockCode, deltaConStr, successCnt, failCnt);
				setDeltaConStrMap(deltaConStrMap, deltaConStr, successCnt, failCnt);
				successCnt = 0;
				failCnt = 0;
			}
		}
		return deltaConStrMap;
	}
	
	/*
	 * 인자값, 순서대로 
	 * 조사할 리스트, 리스트에서
	 */
	public StockContractStrengthDto getNearTimeConStrDto(
			List<StockContractStrengthDto> stockContractStrengthDtoList, int idx, int criteriaSec) {
			StockContractStrengthDto findDto = null;
			
			for(int i = idx-1 ; i >= 0 ; i-- ) {
				findDto = stockContractStrengthDtoList.get(i);
				if(DateUtil.diffSec(stockContractStrengthDtoList.get(idx).getContractTime(),
										findDto.getContractTime()) >= criteriaSec) return findDto;
			}
			
			return null;
	}
	
	public void setDeltaConStrMap(Map<Integer, int[]> deltaConStrMap, int deltaConStr, int successCnt , int failCnt) {
		Integer key = Integer.valueOf(deltaConStr);
		if(deltaConStrMap.containsKey(key)){
			int[] sucFailArr = deltaConStrMap.get(key);
			sucFailArr[0] += successCnt;
			sucFailArr[1] += failCnt;
			deltaConStrMap.put(key, sucFailArr);
		} else {
			deltaConStrMap.put(key, new int[]{successCnt, failCnt});
		}
	}
	
	private void interResult(String stockCode, int deltaConStr, int successCnt , int failCnt ) {
		System.out.println("중간결과 : " + stockCode + ", 강도 : " + deltaConStr);
		System.out.println("총 시도 : " + (successCnt+failCnt) );
		System.out.println("총 성공 : " + successCnt );
		System.out.println("총 실패 : " + failCnt );
		System.out.println("성공률  : " + (successCnt == 0 ? successCnt : successCnt/(successCnt+failCnt) * 100.0) );
	}

}
