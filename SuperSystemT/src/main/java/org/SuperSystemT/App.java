package org.SuperSystemT;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;

import service.stock.StockContractStrengthService;
import service.stock.StockContractStrengthServiceImpl;
import strategy.stock.StockContractStrengthStgy;
import strategy.stock.StockContractStrengthStgyImpl;

/**
 * Hello world!
 *
 */
@ContextConfiguration(locations="/applicationContext.xml")
public class App 
{
	@Autowired
	StockContractStrengthStgy stockContractStrengthStgy;
	@Autowired
	StockContractStrengthService stockContractStrengthService;
	
	public static void main( String[] args )
    {
		ApplicationContext context = new ClassPathXmlApplicationContext("/applicationContext.xml");
		App app = context.getBean(App.class);
		app.analyze();
		/*
		StockContractStrengthService stockContractStrengthService = 
				new StockContractStrengthServiceImpl();
				*/
    }
	
	public void analyze() {
		
		List<String> allCodeList = stockContractStrengthService.getAllStockCodeList();
		
//		stockContractStrengthStgy.analyze(Arrays.asList(new String[]{"A000240"}), 10);
		Map<Integer, int[]> resultMap = stockContractStrengthStgy.analyze(allCodeList, 10);
		
		System.out.println("최종결과 ----");
		for(Integer key : resultMap.keySet()) {
			int[] value = resultMap.get(key);
			System.out.println("KEY : " + key + "successCnt : " + value[0] + "failCnt : " + value[1] + 
										"Success Ratio : " + (value[0]/(double)(value[0]+value[1])) * 100.00 );
		}
	}
}
