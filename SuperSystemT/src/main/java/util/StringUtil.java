package util;

public class StringUtil {
	
	public static String formatToString(String[] printArr, String delim) {
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < printArr.length; i++) {
			if(i == printArr.length -1 ) sb.append(printArr[i]);
			else sb.append(printArr[i] + delim);
		}
		return sb.toString();
	}

}
