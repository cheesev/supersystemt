package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	public static long diffSec(String time1, String time2) throws RuntimeException {
		SimpleDateFormat format = new SimpleDateFormat("HHmmss");	
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = format.parse(time1);
			d2 = format.parse(time2);
		} catch (ParseException e) {
			throw new RuntimeException("time parse error");
		}
		
		return  (d1.getTime() - d2.getTime()) / 1000;
	}
}
