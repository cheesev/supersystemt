package util;

import java.math.BigDecimal;

public class MathUtil {
	/* BigDecimal compareTo method returns 
	 * -1 if the BigDecimal is less than val,
	 * 1 if the BigDecimal is greater than val,
	 * 0 if the BigDecimal is equal to val
	 */
	public  static final BigDecimal defaultCommission = new BigDecimal("5");
	
	public static BigDecimal getNetProfit(BigDecimal profit, BigDecimal commission, int scale) {
		BigDecimal hundred = new BigDecimal("100");
		
		return profit.multiply(
				hundred.subtract(commission).divide(hundred, scale, BigDecimal.ROUND_HALF_UP));
	}
	
	/*
	 * comparedPrice substract commission and price  is positive?
	 */
	public static boolean isPositiveNetProfit(BigDecimal price, BigDecimal comparedPrice, BigDecimal commission, int scale) {
		if( MathUtil.getNetProfit(comparedPrice, commission, scale).subtract(price)
				.compareTo(new BigDecimal("0")) >= 0 ) return true;
		else return false;
	}

	public static boolean isPositiveNetProfit(int price, int comparedPrice, BigDecimal commission, int scale) {
		if( MathUtil.getNetProfit(new BigDecimal(String.valueOf(comparedPrice)), commission, scale).subtract(new BigDecimal(String.valueOf(price)))
				.compareTo(new BigDecimal("0")) >= 0 ) return true;
		else return false;
	}
	
	
	/*
	 *     min < bd <= max 인지 검사하는 함수
	 */
	public static boolean isBetween(BigDecimal bd, int min, int max) {
		
		if( bd.compareTo(new BigDecimal(min)) == 1 
		&& bd.compareTo(new BigDecimal(max)) <= 0 ) return true;
		else return false;
	}

}
