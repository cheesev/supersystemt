package dto.stock;

import java.math.BigDecimal;

import util.StringUtil;

public class StockContractStrengthDto {
	private String stockCode;
	private String ymdDay;
	private String contractTime;
	private int curPrice;
	private BigDecimal contractStrength;
	
	public String getStockCode() {
		return stockCode;
	}
	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}
	public String getYmdDay() {
		return ymdDay;
	}
	public void setYmdDay(String ymdDay) {
		this.ymdDay = ymdDay;
	}
	public String getContractTime() {
		return contractTime;
	}
	public void setContractTime(String contractTime) {
		this.contractTime = contractTime;
	}
	public int getCurPrice() {
		return curPrice;
	}
	public void setCurPrice(int curPrice) {
		this.curPrice = curPrice;
	}
	public BigDecimal getContractStrength() {
		return contractStrength;
	}
	public void setContractStrength(BigDecimal contractStrength) {
		this.contractStrength = contractStrength;
	}
	
	public String toString() {
		return StringUtil.formatToString(new String[]{getStockCode() ,getYmdDay(), getContractTime()
														,String.valueOf(getCurPrice()), getContractStrength().toString()}, " , ");
	}
}
