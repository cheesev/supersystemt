package mapper.stock;

import java.util.List;
import java.util.Map;

import dto.stock.StockContractStrengthDto;

public interface StockContractStrengthMapper {
	List<StockContractStrengthDto> getStockContractStrengthDtoListByCode(String stockCode);
	List<String> getAllStockCodeList();
	StockContractStrengthDto getLastTimeStockContractStrengthDto(String stockCode);
}
